package com.example.exercise04;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private List<ContactModel> contactList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();
        listView = findViewById(R.id.lvContact);
        ContactAdapter adapter = new ContactAdapter(contactList, this);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ContactModel contactModel = contactList.get(position);
                Toast.makeText(MainActivity.this, contactModel.getName(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initData() {
        ContactModel contactModel = new ContactModel("Nguyen Van A", "09123123", R.drawable.ic_user_a);
        contactList.add(contactModel);
        contactModel = new ContactModel("Nguyen Van B", "09321321", R.drawable.ic_user_a);
        contactList.add(contactModel);
        contactModel = new ContactModel("Nguyen Van C", "09321321", R.drawable.ic_user_a);
        contactList.add(contactModel);
        contactModel = new ContactModel("Nguyen Van D", "09321321", R.drawable.ic_user_a);
        contactList.add(contactModel);
        contactModel = new ContactModel("Nguyen Van E", "09321321", R.drawable.ic_user_a);
        contactList.add(contactModel);
        contactModel = new ContactModel("Nguyen Van F", "09321321", R.drawable.ic_user_a);
        contactList.add(contactModel);
        contactModel = new ContactModel("Nguyen Van G", "09321321", R.drawable.ic_user_a);
        contactList.add(contactModel);
        contactModel = new ContactModel("Vuong Gia Bao", "098765432", R.drawable.ic_user_a);
        contactList.add(contactModel);
    }
}